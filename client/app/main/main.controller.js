'use strict';

angular.module('doctorsitoApp')
  .controller('MainCtrl', function ($scope, $http, socket) {
    $scope.history = [];
    $scope.connections = [];
    $scope.actions = {discover:true};
    $scope.items = [];
    $scope.inventari = [];

   socket.on("update", function(data)
	{
		data.forEach(function(elem) {
			consolelog("text",elem);
			
		});
	});

	socket.on("room_changed", function(data)
	{
		consolelog("text",data.enter_message);
		$scope.connections = data.connections;
		$scope.items = data.items;
	});	

	$scope.change_room = function($this)
	{
		socket.emit("change_room", $this.conn.target_room);
	}

	$scope.discover = function()
	{
		
		if($scope.items.length>0)
		{
			consolelog("text","Mirant una mica has trobat:");
			consolelog("items",$scope.items);
		}
		else consolelog("No hi ha res destacable");
	}

	$scope.pickup_item = function($this)
	{
		consolelog("text","Agafes "+$this.item.name);
		$scope.inventari.push($this.item);
	}

	function consolelog(sType,mValue)
	{
		$scope.history.push({type:sType,value:mValue});
		if($scope.history.length>38) $scope.history.splice(0,1);
	} 


});

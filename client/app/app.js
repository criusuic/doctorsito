'use strict';

angular.module('socketService', []).
  factory('socket', function($rootScope) {
    var socket = io.connect();
    console.log("Em connecto");
    return {
      on: function (eventName, callback) {
        socket.on(eventName, function() {
          var args = arguments;
          $rootScope.$apply(function() {
            callback.apply(socket, args);
          });
        });
      },
      emit: function(eventName, data, callback) {
        socket.emit(eventName, data, function() {
          var args = arguments;
          $rootScope.$apply(function() {
            if(callback) {
              callback.apply(socket, args);
            }
          });
        });
      }
    };
  });

  

angular.module('doctorsitoApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'socketService'
])
  .config(function ($routeProvider, $locationProvider, $sceProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });
    $sceProvider.enabled(false);
    $locationProvider.html5Mode(true);
  });
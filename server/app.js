/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config/environment');
// Setup server
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
require('./config/express')(app);
require('./routes')(app);

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

io.sockets.on('connection', function (socket)
{
		socket.emit("update", ["Benvingut a Dktr"]);	
		initGame(socket);	
		applyEvents();

		socket.on("change_room", function(room)
		{
			oGame.oCurrentRoom = oGame.indexed_rooms[room];
			
			socket.emit("room_changed",oGame.oCurrentRoom);
			applyEvents();
		})
});

var bUpdateRequired = true;
var aUpdateList = [];
setInterval(function() {
		if(bUpdateRequired)
		{
			io.emit("update", aUpdateList);
			bUpdateRequired	= false;
			aUpdateList = [];
		}
},100);

setInterval(function() {
	/*aUpdateList.push(Math.random(50));
	bUpdateRequired=true;*/
	/*if(Math.random()>0.9)
	{
		aUpdateList.push("Tempesta màxima!!!");
		bUpdateRequired=true;
	}*/
	//updatePlayerStats();
	
},500);

var oGame = require("./game.json");
var aMsgs = require("./messages.json");

function initGame(socket)
{
	initRooms(oGame);
	oGame.oCurrentRoom = oGame.indexed_rooms[oGame.initial_room];
	//console.log(oGame.oCurrentRoom);

	socket.emit("room_changed",oGame.oCurrentRoom);
}

function applyEvents()
{
	oGame.events.forEach(function(oEvent) {
		var bRes = eval_conditions(oEvent.conditions);
		if(bRes)
		{
			applyActions(oEvent.actions);
		}
	});
}

function applyActions(aActions)
{
	aActions.forEach(function(oAction)
	{
		if(oAction.action=='console_write')
		{
			aUpdateList.push(aMsgs[oAction.msg]);
			bUpdateRequired = true;
		}
	})
}
function eval_conditions(aConditions)
{
	var bCompleix = true;
	var aKeys = Object.keys(aConditions);
	for(var i=0;i<aKeys.length;i++)
	{
		var sKey = aKeys[i];
		var mCondition = aConditions[sKey];
		if(sKey==='current.room')
		{
			if(oGame.oCurrentRoom.id !== mCondition) 
			{
				bCompleix = false;
				break;
			}
		} 
	}
	return bCompleix;
}
// Expose app

exports = module.exports = app;



function initRooms(oGame)
{
	var aRooms = oGame.map.rooms;
	var aEvents = [];
	var aIndexedRooms = [];
	aRooms.forEach(function(oRoom)
	{
		aIndexedRooms[oRoom.id] = oRoom;
		aEvents.push(firstTimeRoomText(oRoom.id,oRoom.id+'.first_message'));
	});
	oGame.events = aEvents;
	oGame.indexed_rooms = aIndexedRooms;
	
}

function firstTimeRoomText(sRoomId, sMessageId)
{
	var oEvent = {
			      "conditions":{
			          "current.room":sRoomId,
			          "times_visited":{"comparator":"eq", "value":0}
			      },
			      "actions": [
			      		{"action":"console_write","msg":sMessageId}
			      ]
			    };
			    console.log(oEvent);
	return oEvent;
}